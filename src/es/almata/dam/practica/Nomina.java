package es.almata.dam.practica;

public class Nomina {
	private Empresa empresa;
	private Treballador treballador;
	private int ID;
	private String mes;
	private int any;
	private double totalNomina;
	
	
	public Nomina() {
		super();
	}
	public Nomina(int iD, String mes, int any) {
		super();
		ID = iD;
		this.mes = mes;
		this.any = any;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public int getAny() {
		return any;
	}
	public void setAny(int any) {
		this.any = any;
	}
	public double getTotalNomina() {
		return totalNomina;
	}
	public void setTotalNomina(double totalNomina) {
		this.totalNomina = totalNomina;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Treballador getTreballador() {
		return treballador;
	}
	public void setTreballador(Treballador treballador) {
		this.treballador = treballador;
	}
	
	
	@Override
	public String toString() {
		return " ******* Nomina ******\n    Mes: " + mes + "\n    Any: " + any;
	}
	
	

}
