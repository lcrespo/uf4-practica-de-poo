package es.almata.dam.practica;

public class Operari extends Treballador {
	
	
	public Operari() {
		super();
	}

	public Operari(String dNI, String nom, int horesTreballades) {
		super(dNI, nom, horesTreballades);
	}

	public int preuHora() {
		System.out.println("   - Cobra 10€/hora");
		return 10;
	}


	@Override
	public String toString() {
		return "\n Treballa com a Operari" + super.toString();
	}
	
}
