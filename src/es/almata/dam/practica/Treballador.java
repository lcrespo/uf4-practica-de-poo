package es.almata.dam.practica;

import java.util.ArrayList;

public abstract class Treballador {
	private String DNI;
	private String nom;
	private int horesTreballades;
	private Nomina nomina;
	private ArrayList<Nomina>nomines = new ArrayList<Nomina>();
	
	public abstract int preuHora();
	
	
	public Treballador() {
		super();
	}
	public Treballador(String dNI, String nom, int horesTreballades) {
		super();
		DNI = dNI;
		this.nom = nom;
		this.horesTreballades=horesTreballades;
	}
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getHoresTreballades() {
		return horesTreballades;
	}
	public void setHoresTreballades(int horesTreballades) {
		this.horesTreballades = horesTreballades;
	}
	public Nomina getNomina() {
		return nomina;
	}
	public void setNomina(Nomina nomina) {
		this.nomina = nomina;
	}
	public ArrayList<Nomina> getNomines() {
		return nomines;
	}
	public void setNomines(ArrayList<Nomina> nomines) {
		this.nomines = nomines;
	}
	
	@Override
	public String toString() {
		return "\n   DNI: " + DNI + "\n   Nom: " + nom;
	}

	
}
