package es.almata.dam.practica;

public class Administratiu extends Treballador {
	
	public Administratiu() {
		super();
	}

	public Administratiu(String dNI, String nom, int horesTreballades) {
		super(dNI, nom,horesTreballades);
	}

	@Override
	public int preuHora() {
		System.out.println("   - 15€/hora");
		return 15;
	}

	@Override
	public String toString() {
		return "\n Treballa com a Administratiu" + super.toString() + "\n   Hores treballades: " +  getHoresTreballades() + "h" ;
	}

	
}
