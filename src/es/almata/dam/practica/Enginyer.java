package es.almata.dam.practica;

public class Enginyer extends Treballador {

	@Override
	public int preuHora() {
		System.out.println("   - Cobra 25€/hora");
		return 25;
	}
	
	public Enginyer() {
		super();
	}
	public Enginyer(String dNI, String nom,int horesTreballades) {
		super(dNI, nom, horesTreballades);
	}
	
	@Override
	public String toString() {
		return "\n Treballa com a Enginyer" + super.toString() + "\n   Hores treballades: " +  getHoresTreballades() + "h" ;
	}

}
