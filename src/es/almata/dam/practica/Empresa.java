package es.almata.dam.practica;

import java.util.ArrayList;

public abstract class Empresa {
	private Nomina nomina;
	private Treballador treballador;
	ArrayList<Treballador>treballadors = new ArrayList<Treballador>();
	private String NIF;
	private String nom;
	private String adreça;
	private ArrayList<Nomina> nomines;
	
	
	public Empresa() {
		super();
	}
	public Empresa(String nIF, String nom, String adreça) {
		super();
		NIF = nIF;
		this.nom = nom;
		this.adreça = adreça;
	}
	public String getNIF() {
		return NIF;
	}
	public void setNIF(String nIF) {
		NIF = nIF;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdreça() {
		return adreça;
	}
	public void setAdreça(String adreça) {
		this.adreça = adreça;
	}
	
	public Nomina getNomina() {
		return nomina;
	}
	public void setNomina(Nomina nomina) {
		this.nomina = nomina;
	}
	public Treballador getTreballador() {
		return treballador;
	}
	public void setTreballador(Treballador treballador) {
		this.treballador = treballador;
	}
	public ArrayList<Nomina> getNomines() {
		return nomines;
	}
	public void setNomines(ArrayList<Nomina> nomines) {
		this.nomines = nomines;
	}
	
	public double calcularNomina(Treballador treballador, Nomina nomina2) {
		int preuHora=treballador.preuHora();
		int horesTreballades=treballador.getHoresTreballades();
		double totalNomina = preuHora*horesTreballades;
		System.out.println(" - Total de la nomina: " + totalNomina);
		return totalNomina;
		
	}
	
	public void donarNomina(Nomina nomina, Treballador treballador) {
		System.out.println("Nòmina donada");
		treballadors.add(treballador);
		nomines.add(nomina);
	}
	
	@Override
	public String toString() {
		return "******************* Empresa ******************* \n NIF:" + NIF + "\n Nom" + nom + "\n Adreça:" + adreça + "\n";
	}

	
	
	
}
