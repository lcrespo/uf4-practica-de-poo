package es.almata.dam.practica;

import java.util.ArrayList;

public class Run {

	public static void main(String[] args) {
	
		Empresa empresa = new Empresa("J29746401","No hay paraíso","Av/Lluís Comanys 25, Balaguer 25600") {
		};
		System.out.println(empresa);
		
		// Treballadors Enginyers 
		Enginyer enginyer = new Enginyer("35017382D","Miguel Àngel Silvestre",40);
		Enginyer enginyer2=new Enginyer("49320315N","Amaia Salamanca",36);
		
		
		Nomina nomina = new Nomina(1,"Gener",2021);
		Nomina nomina2 = new Nomina(2,"Gener",2021);
		
		System.out.println(enginyer2);
		System.out.println(nomina2);
		empresa.calcularNomina(enginyer2, nomina2);
		
		System.out.println(enginyer);
		System.out.println(nomina);
		empresa.calcularNomina(enginyer, nomina);
	
		// Treballadors Operaris 
		Treballador treballador = new Operari("49531722M","María Castro",40);
		Treballador treballador2 = new Operari("36729516S","Thaïs Blume",20);
		
		// Treballadors Administratius 
		Treballador treballador4=new Administratiu("39726417D","Luis Zahera",38);
		Treballador treballador5=new Administratiu("46197364V","Cuca Escribano",15);
		
		ArrayList<Treballador> treballadors6= new ArrayList<Treballador>();
		treballadors6.add(treballador);
		treballadors6.add(treballador2);
		treballadors6.add(treballador4);
		treballadors6.add(treballador5);
		
		
		ArrayList<Nomina> nomines = new ArrayList<Nomina>();
		nomines.add(nomina);
		nomines.add(nomina2);
		
		// recorrer amb foreach
			for (Treballador treballadors : treballadors6) {
				calcNominaTreb(empresa, treballadors, nomina);
			} 
			
	}
	
	public static void calcNominaTreb(Empresa e, Treballador t, Nomina n) {
		System.out.println(t);
		System.out.println(n);
		e.calcularNomina(t, n);
	}
	
	public static void donarNomimaTReb(Empresa e, Treballador t, Nomina n) {
		e.donarNomina(n, t);
	}

}
